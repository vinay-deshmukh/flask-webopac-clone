from flask import Flask, render_template, request, make_response, session
from flask import redirect, url_for
app = Flask(__name__)

from collections import namedtuple, OrderedDict
SESSION_ID = 'session'

sessionList = []

mySessionDict = {}
# Use this instead of the flask.session
# since it encrypts the keys, and thus breaking the
# if key in session 
# checks

class Book:
    # static constant
    CAN_RENEW = 'can'
    CANNOT_RENEW = 'cannot'

    def __init__(self, acc_no='B1234', title='Title', 
                       due='12/12/12', fine='0.0', 
                       renewCount='0', reserve='0', 
                       renew=CAN_RENEW):
        self.acc_no = acc_no
        self.title = title
        self.due = due
        self.fine = fine
        self.renewCount = renewCount
        self.reserve = reserve
        self.renew = renew

        # dict of tuples of (name, value)
        # keys are 'acc_no', 'm_media', 'i_num'
        # values ('acc_noNUM', '1234'), ('m_mediaNUM', '1'), NUM
        # this will be used for input tags: m_accno{NUM} and m_media{NUM}
        # they need to generated based on total number of reissueable books
        self.extras = dict()
    
    def asOrderedDict(self):
        od = OrderedDict()
        od['acc_no'] = self.acc_no
        od['title'] = self.title
        od['due'] = self.due
        od['fine'] = self.fine
        od['renewCount'] = self.renewCount
        od['reserve'] = self.reserve
        od['renew'] = self.renew
        return od




#TupUser = namedtuple('TupUser', 'pid pwd first_name last_name fathers_name mothers_name')

def TupUser(pid, pwd, first_name, last_name, fathers_name, mothers_name, books=None):
    d = {}
    d['pid'] = pid
    d['pwd'] = pwd
    d['first_name'] = first_name
    d['last_name'] = last_name
    d['fathers_name'] = fathers_name
    d['mothers_name'] = mothers_name
    if books is None:
        books = [Book('Kumbho'), Book('Techmax')]
    d['books'] = books
    return d

def check_if_registered_user(pid, pwd):
            for user in tupleUsers:
                if pid == user['pid'] and pwd == user['pwd']:
                    return user
            return None

def get_user_by_pid(pid):
    for user in tupleUsers:
        if pid == user['pid']:
            return user
    return None

tupleUsers = ( TupUser(pid='1', pwd='1', 
                      first_name='Harry', last_name='Potter',
                      fathers_name='James', mothers_name='Lily',
                      books=[Book(title="Fifty Shades of Grey"),
                             Book(title="The Last Wish"),
                             Book(title="Hunger Games", renew=Book.CANNOT_RENEW)]
                # Harry: One book is overdue
                      ),

               TupUser(pid='2', pwd='2',
                      first_name='Hermione', last_name='Granger',
                      fathers_name='Daniel', mothers_name='Jean',
                      books=[Book(title="Structured Programming Approach"),
                             Book(title="The Architect")]
                # Hermione: Both books can be renewed
                # But, we don't know what will be the name attribute for the checkbox tags
                      ),

                TupUser(pid='3', pwd='3',
                        first_name='Ronald', last_name='Weasley',
                        fathers_name='Bilius', mothers_name='Molly',
                        books=[Book(title='Marvel #13', renew=Book.CANNOT_RENEW),
                               Book(title="Galvin", renew=Book.CANNOT_RENEW)]
                # Ron: Both books are overdue
                # So, we can perform no further action
                        ),
                
                TupUser(pid='4', pwd='4',
                        first_name='Draco', last_name='Malfoy',
                        fathers_name='Lucius', mothers_name='Narcissa',
                        books=[]) 
                # Draco: No books borrowed
                # For case of no books borrowed by student
                # We pass empty list for books,
                # since None will mean a default list of books defined in the Book constructor

             )

@app.route("/")
def index():
    resp = make_response(render_template("index.html"))

    # assign session ID to current visitor
    session_id = str(len(sessionList) + 1) + " SESSION ID"
    resp.set_cookie(SESSION_ID, session_id)

    # add the session_id to sessionList
    sessionList.append(session_id)

    return resp

# page after being logged in
'http://115.248.171.105:82/webopac/opac_s.asp?m_memchk_flg=&m_summary=Y'

# page after incorrect log in
'http://115.248.171.105:82/webopac/opac_s.asp?m_memchk_flg=IMI&m_summary=N'

# form action to login
@app.route('/opac.asp?m_firsttime=Y&m_memchk_flg=T', methods=['GET', 'POST'])
@app.route('/userpage', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        print("POST")
        # Check the 4 inputs
        '''
        input:  name       value
        PID:    m_mem_id   ''
        Pass:   m_mem_pwd  ''
        Submit: Submit     Submit
        Reset:  Submit2    Reset
        '''

        pid = request.form['m_mem_id']
        pwd = request.form['m_mem_pwd']


        currUser = None
        currUser = check_if_registered_user(pid, pwd)

        if currUser is not None:
            # Store currUser to session
            #session['currUser'] = (currUser['pid'], currUser['pwd'])

            # Assign latest sessionId to currUser
            # Associate pid to the session_id
            mySessionDict[sessionList[-1]] = pid

            resp = make_response(render_template('profile.html', 
                                   first_name=currUser['first_name'].upper(),
                                   last_name=currUser['last_name'].upper(),
                                   fathers_name=currUser['fathers_name'].upper(),
                                   mothers_name=currUser['mothers_name'].upper(),
                                   sep=' ')
                                )
            
            #resp.set_cookie(COOKIE_NAME, currUser['pid'])
            return resp
        else:
            return render_template('invalid_login.html')

    elif request.cookies.get(SESSION_ID) in sessionList:
        # if 
        # if user is logged in
        # go to userpage
        
        # obtain the session_id of the request
        session_id = request.cookies.get(SESSION_ID)

        # Retrieve the associated pid
        pid = mySessionDict.get(session_id)

        if pid == None:
            return redirect(url_for('index'))

        currUser = get_user_by_pid(pid)

        if currUser == None:
            return redirect(url_for('index'))

        return render_template('profile.html', 
                                   first_name=currUser['first_name'].upper(),
                                   last_name=currUser['last_name'].upper(),
                                   fathers_name=currUser['fathers_name'].upper(),
                                   mothers_name=currUser['mothers_name'].upper(),
                                   sep=' ')
    else:
        # Redirect to index if request is not a POST
        return redirect(url_for('index'))

@app.route('/out_docs')
def out_docs():
    # check if logged in
    if request.cookies.get(SESSION_ID) not in sessionList:
        #if session.get('currUser') is None:
        # not logged in, so go to index
        return redirect(url_for('index'))
    else:
        # user is logged in
        # let him see the books

        # get the session_id
        session_id = request.cookies.get(SESSION_ID)

        # get associated pid
        pid = mySessionDict.get(session_id)

        if pid == None:
            return redirect(url_for('index'))

        currUser = get_user_by_pid(pid)
        
        if currUser == None:
            return redirect(url_for('index'))

        if len(currUser['books']) > 0:
            # Render the out_per_user.html
            # ie the outstanding docs page with books
            # but only if user has atleast one book

            # Figure out the tag names and details    
            # find no of books that can be reissued
            m_count = 0
            for book in currUser['books']:
                if book.renew == Book.CAN_RENEW:
                    m_count += 1
                
                # Clear out the extras dict, before generating it again in the first loop
                book.extras.clear()
            
            i_num = 1 # use this to set extras variable for reissueable books
            for book in currUser['books']:
                if book.renew == Book.CAN_RENEW:
                    book.extras['acc_no'] = ('m_accno' + str(i_num), # input tag name = m_accno1
                                            book.acc_no[1:])         # input tag value = "1234", when acc_no = B1234
                                            
                                            
                    book.extras['m_media'] = ('m_media' + str(i_num), # input tag name = m_media1
                                            '1')                   # input tag value = "1", this is same always
                    book.extras['i_num'] = i_num
                    i_num += 1




            return render_template('out_per_user.html', 
                               books=currUser['books'], 
                               Book=Book, # pass Book class to get access to constant
                               m_count=m_count)
        else:
            return render_template("no_borrowed_books.html")

@app.route('/renew_success', methods=['GET', 'POST'])
def renew_success():
    # check if logged in
    if request.cookies.get(SESSION_ID) not in sessionList:
        #if session.get('currUser') is None:
        # not logged in, so go to index
        return redirect(url_for('index'))
    else:
        # user is logged in
        # let him see the books

        # get the session_id
        session_id = request.cookies.get(SESSION_ID)

        # get associated pid
        pid = mySessionDict.get(session_id)

        if pid == None:
            return redirect(url_for('index'))

        currUser = get_user_by_pid(pid)
        
        if currUser == None:
            return redirect(url_for('index'))
        # above copied from out_docs

        # The only traffic coming here will be POST
        
        # Reissuing the books code section
        for book in currUser['books']:
            if book.renew == Book.CAN_RENEW:
                # find the name of checkbox for current book
                chk_name =  'm_chk' + str(book.extras['i_num'])
                print("CHK: " + chk_name)

                print(request.form)

                if chk_name in request.form:
                    # if checkbox name is in the request form
                    # then reissue that book

                    print("IN REQUEST")

                    # Set book as un reissueable
                    book.renew = Book.CANNOT_RENEW
                    book.due = '1/1/2020'



        # debug
        d_post = request.form

        return render_template('renew_success.html', d_post=d_post)
    

@app.before_first_request
def init_app():
    global mySessionDict
    #session.clear()
    mySessionDict = {}


if __name__ == '__main__':
    # app.config['SECRET_KEY'] = 'bingbongtheorem'
    # clear sessio for everyrun
    app.run(debug=True)
